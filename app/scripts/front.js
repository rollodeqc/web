(function() {
  'use strict';

  var microsondageEl = document.getElementById('microsondage');
  var optEl;
  var formEl;

  if (microsondageEl) {
    optEl = microsondageEl.getElementsByTagName('fieldset')[0];
    optEl.setAttribute('class', 'hidden');
    microsondageEl.getElementsByTagName('input')[0].addEventListener('focus', function() {
      optEl.setAttribute('class', 'animated zoomInRight');
    });

    formEl = microsondageEl.getElementsByTagName('form')[0];
    formEl.addEventListener('submit', function (e) {
      var smoothScroll = function smoothScroll() {
        if (window.scrollY < 0) {
          window.scroll(0, 0);
        } else if (window.scrollY) {
          window.scrollByLines(-Math.ceil(window.scrollY / 300));
          setTimeout(smoothScroll, 0);
        }
      };

      var merci = function merci(form, msg) {
        var merciEl = document.createElement('p');
        var okEl = document.createElement('button');
        if (!msg) { msg = 'Merci, votre support est apprécié.'; }
        form.removeEventListener('animationend', merci);
        merciEl.innerHTML = msg;
        merciEl.setAttribute('class', 'merci animated pulse');
        okEl.innerHTML = 'OK';
        okEl.setAttribute('class', 'ok-dismiss');
        okEl.addEventListener('click', function () {
          merciEl.setAttribute('class', 'merci animated zoomOutRight');
          merciEl.addEventListener('animationend',
            microsondageEl.removeChild.bind(microsondageEl, merciEl));
        });
        merciEl.appendChild(okEl);
        microsondageEl.replaceChild(merciEl, form);
      };

      var readForm = function (form) {
        var inputEls = form.getElementsByTagName('input');
        var ret = [];
        var textareaEl = form.getElementsByTagName('textarea')[0];
        var r;
        var z;

        if (textareaEl && textareaEl.value) {
          ret.push(encodeURIComponent(textareaEl.name) +
            '=' + encodeURIComponent(textareaEl.value));
        }
        for (r = 0; r < inputEls.length; ++r) {
          switch (inputEls[r].type) {
          case 'email':
          case 'text':
            z = true;
            break;

          case 'radio':
          case 'checkbox':
            z = inputEls[r].checked;
            break;

          default:
            z = false;
          }
          if (z && inputEls[r].name && inputEls[r].value) {
            ret.push(encodeURIComponent(inputEls[r].name) +
              '=' + encodeURIComponent(inputEls[r].value));
          }
        }
        return ret.join('&');
      };

      var sendForm = function(form) {
        var myRequest = new XMLHttpRequest();
        var loadCb = function () {
          form.addEventListener('animationend',
            merci.bind(null, form,
              this.status >= 200 && this.status < 400 ?
                this.response :
                ('Erreur ' + this.statusText + ' (' + this.status +
                '). Veuillez contacter l\'administrateur si le problème persiste: robin@millette.info')
            )
          );
        };
        myRequest.addEventListener('load', loadCb);
        myRequest.addEventListener('error', loadCb);

        myRequest.open('PUT', form.action, true);
        myRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        myRequest.send(readForm(form));
      };

      e.preventDefault();
      sendForm(formEl);
      smoothScroll();
      formEl.setAttribute('class', 'animated zoomOutRight');
    });
  }
}());
